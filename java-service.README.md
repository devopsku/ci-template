# Java Service CI Templates

This template using 2 file:
1. ./dockerfiles/java-service.Dockerfile, for Dockerfile
   This dockerfile has two stage:
   1. Maven stage for downloading maven artifact that build by CI build job and also downloading necessary  libs like hazelcast and apm-agent
   2. Main Stage using default adoptopenjdk/openjdk11-openj9:x86_64-alpine-jre-11.0.10_9_openj9-0.24.0, which run java command with spesifiying JVM options
2. ./templates/java-service.gitlab-ci.yml, for defining CI stage and job
   This file contain 4 stages
   1. Build stage for running maven clean deploy (package and publish to nexus repo)
   2. Test stage for Code review using sonarqube
   3. Release stage for building docker image using spesific dockerfile
   4. Security stage for image scanning
   5. Deploy stage for deploying to kubernetes
