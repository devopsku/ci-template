# Java Service CI Templates

This template using 3 file:
1. ./dockerfiles/angularjs.Dockerfile, for Dockerfile
   This dockerfile has two stage:
   1. Node stage for downloading node artifact that build by CI build job 
   2. Main Stage using default nginx:1.18-alpine, which run nginx
2. ./scripts/nginx.default.conf for nginx configuration
3. ./templates/angularjs.gitlab-ci.yml, for defining CI stage and job
   This file contain 2 stages
   1. Release stage for building docker image using spesific dockerfile
   2. Security stage for image scanning
   3. Deploy stage for deploying to kubernetes
