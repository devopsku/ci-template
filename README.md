# This is project for hosting CI templates

Currently there is 3 major directory for enabling CICD
1. dockerfiles, which contain default dockerfiles for many deployment type, for example java-service should using `java-service.Dockerfile`
2. scripts, which contain bash script that can be use for entrypoint or utility script, for example java-service should using `java-service.entrypoint.sh`
3. templates, which contain CICD flow that define job that will run on gitlab-runner, for example java-service should using `java-service.gitlab-ci.yml`

This CICD flow can be called from another project by referencing it in the project owned `.gitlab-ci.yml`, like below example:
```yaml
include:
- project: devops/ci-templates
  ref: &include_ref 1.0.0-SNAPSHOT
  file: /templates/java-service.gitlab-ci.yml

variables:
  CI_PROJECT_NAMESPACE: account-tribe
  FILE_TAG: *include_ref
``` 
Each project can overide job, variable, script etc by defining their own value by editing .gitlab-ci.yml like sample above, which 
overide variable `CI_PROJECT_NAMESPACE` to core-services (template default is business-services). For spesific template please refer 
to respective CI readme
