# Golang CI Templates

This template using 2 file:
1. ./dockerfiles/golang.Dockerfile, for Dockerfile
   This dockerfile has two stage:
   1. golang stage for downloading go artifact that build by CI build job 
   2. Main Stage using default alpine to add golang binary
2. ./templates/golang.gitlab-ci.yml, for defining CI stage and job
   This file contain 4 stages
   1. Build stage for running maven clean deploy (package and publish to nexus repo)
   2. Release stage for building docker image using spesific dockerfile
   3. Security stage for image scanning
   4. Deploy stage for deploying to kubernetes
