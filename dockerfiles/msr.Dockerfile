ARG IMAGE_TAG="10.11.0.0"
FROM nexus-rancher.banksinarmas.com:18086/store/softwareag/webmethods-microservicesruntime:${IMAGE_TAG}

USER root
COPY ./assets/IS/Packages/ /opt/softwareag/IntegrationServer/packages/
COPY ./licenseKey.xml /opt/softwareag/IntegrationServer/config/licenseKey.xml
COPY ./scripts/*build.sh /opt/softwareag/IntegrationServer/
RUN chmod +x /opt/softwareag/IntegrationServer/*.sh
COPY ./application.properties /opt/softwareag/IntegrationServer/application.properties

RUN chown -R sagadmin:sagadmin /opt/softwareag/IntegrationServer

USER sagadmin
RUN if [ -f "/opt/softwareag/IntegrationServer/build.sh" ]; \
  then /opt/softwareag/IntegrationServer/build.sh; fi