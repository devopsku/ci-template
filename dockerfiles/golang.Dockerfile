# Stage build
FROM nexus-rancher.banksinarmas.com:18086/bsim/golang:1.19.1-alpine3.16 as builder

ARG GROUP_NAME=revamp-core
ARG SERVICE_NAME=skeleton
ARG BUILD_PATH=/go/src/gitlab.banksinarmas.com/${GROUP_NAME}/${SERVICE_NAME}

ENV PATH $GOPATH/bin:/usr/local/go/bin:$PATH

# compile go project
WORKDIR ${BUILD_PATH}
COPY . .
RUN GO111MODULE=on GOPRIVATE=gitlab.banksinarmas.com go mod download
RUN CGO_ENABLED=1 GOARCH=amd64 GOOS=linux go build -a -o ${SERVICE_NAME} -tags=static_all,musl .

# Create sinarmas
ENV USER=sinarmas
ENV UID=10001

# See https://stackoverflow.com/a/55757473/12429735RUN
RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    "${USER}"

# Stage Runtime Applications
FROM nexus-rancher.banksinarmas.com:18086/bsim/alpine:3.16.2

ARG COPYFOLDER

ARG SERVICE_NAME=skeleton
ARG GROUP_NAME=revamp-core
ARG BUILD_PATH=/go/src/gitlab.banksinarmas.com/${GROUP_NAME}/${SERVICE_NAME}
ARG RUNTIME_PATH=/opt/${SERVICE_NAME}
ENV SERVICE_COMMAND ${RUNTIME_PATH}/${SERVICE_NAME}
WORKDIR ${RUNTIME_PATH}

# Setting timezone
ENV TZ=Asia/Jakarta
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# need to be update
ENV BUILDDIR /go/src/gitlab.banksinarmas.com/${GROUP_NAME}/${SERVICE_NAME}

# Setting folder workdir
WORKDIR ${RUNTIME_PATH}

# Copy Data App
COPY --from=builder ${BUILD_PATH}/${SERVICE_NAME} main-app
COPY --from=builder ${BUILD_PATH}/${COPYFOLDER} ${COPYFOLDER}/
# Copy user data
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group

# Setting owner file and dir
RUN mkdir /var/log/filebeat \
    && chown -R sinarmas:sinarmas . \
    && chown -R sinarmas:sinarmas /var/log/filebeat


USER sinarmas

EXPOSE 8081

CMD [ "./main-app", "serve" ]