# Install dependencies only when needed
FROM nexus-rancher.banksinarmas.com:18086/bsim/node:16.18-alpine3.16 AS deps
# Check https://github.com/nodejs/docker-node/tree/b4117f9333da4138b03a546ec926ef50a31506c3#nodealpine to understand why libc6-compat might be needed.

WORKDIR /app

# Create sinarmas
ENV USER=sinarmas
ENV UID=10001

# See https://stackoverflow.com/a/55757473/12429735RUN
RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    "${USER}"

# Install dependencies based on the preferred package manager
COPY package.json yarn.lock* package-lock.json* pnpm-lock.yaml* ./
RUN \
  if [ -f yarn.lock ]; then yarn --frozen-lockfile; \
  elif [ -f package-lock.json ]; then npm ci; \
  elif [ -f pnpm-lock.yaml ]; then yarn global add pnpm && pnpm i; \
  else echo "Lockfile not found." && exit 1; \
  fi

# Rebuild the source code only when needed
FROM nexus-rancher.banksinarmas.com:18086/bsim/node:16.18-alpine3.16 AS builder
WORKDIR /app
COPY --from=deps /app/node_modules ./node_modules
COPY . .

# Next.js collects completely anonymous telemetry data about general usage.
# Learn more here: https://nextjs.org/telemetry
# Uncomment the following line in case you want to disable telemetry during the build.
ENV NEXT_TELEMETRY_DISABLED 1

RUN yarn build

# If using npm comment out above and use below instead
# RUN npm run build


# Production image, copy all the files and run next
FROM nexus-rancher.banksinarmas.com:18086/bsim/node:16.18-alpine3.16 AS runner
WORKDIR /app

ENV NODE_ENV production
# Uncomment the following line in case you want to disable telemetry during runtime.
ENV NEXT_TELEMETRY_DISABLED 1

# Copy user data
COPY --from=deps /etc/passwd /etc/passwd
COPY --from=deps /etc/group /etc/group

COPY --from=builder /app/public ./public

# Automatically leverage output traces to reduce image size
# https://nextjs.org/docs/advanced-features/output-file-tracing
COPY --from=builder --chown=sinarmas:sinarmas /app/.next/standalone ./
COPY --from=builder --chown=sinarmas:sinarmas /app/.next/static ./.next/static


# Setting owner file and dir
RUN mkdir /var/log/filebeat \
    && chown -R sinarmas:sinarmas . \
    && chown -R sinarmas:sinarmas /var/log/filebeat

USER sinarmas

EXPOSE 3000

ENV PORT 3000

CMD ["node", "server.js"]
