ARG IMAGE_NAME="bsim/openjdk11-openj9"
ARG IMAGE_TAG="x86_64-alpine-jre-11.0.10_9_openj9-0.24.0"
FROM nexus-rancher.banksinarmas.com:18086/bsim/maven:3.6.1-jdk-8-alpine as download-stage

ARG ARTIFACT
ARG MVN_JARS

WORKDIR /app

# Create appuser
ENV USER=appuser
ENV UID=10001

# See https://stackoverflow.com/a/55757473/12429735RUN
RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    "${USER}"

RUN for MVN_JAR in $MVN_JARS; do \
    mvn org.apache.maven.plugins:maven-dependency-plugin:3.1.2:copy \
      -Dartifact=${MVN_JAR} \
      -DoutputDirectory=. \
      -Dmdep.useBaseVersion=true; \
    done;

RUN mvn org.apache.maven.plugins:maven-dependency-plugin:3.1.2:copy \
      -Dartifact=${ARTIFACT} \
      -DoutputDirectory=. \
      -Dmdep.useBaseVersion=true

FROM nexus-rancher.banksinarmas.com:18086/${IMAGE_NAME}:${IMAGE_TAG}

ARG APP_WAR
ARG MVN_GRPID
ARG APK_LIST

ENV MVN_GRPID="${MVN_GRPID}"
ENV JVM_OPTS=""

COPY --from=download-stage /app/ /app/
COPY --from=download-stage /etc/passwd /etc/passwd
COPY --from=download-stage /etc/group /etc/group

COPY ./ci-files* /app/

RUN apk add --no-cache ${APK_LIST} \
    && mv /app/${APP_WAR} /app/main.jar \
    && mkdir /var/log/filebeat \
    && chown -R appuser:appuser /var/log/filebeat \
    && chown -R appuser:appuser /app \
    && ls -la /app

RUN if [ -f "/app/build.sh" ]; then \
      chmod +x /app/build.sh; \
      /app/build.sh; \
    fi

# Use an unprivileged user.
USER appuser:appuser

WORKDIR /app
EXPOSE 8089
CMD java -XX:+TieredCompilation -XX:TieredStopAtLevel=1 \
    -XX:CICompilerCount=2 -Djava.awt.headless=true -Xss228k -Xms256m -Xmx256m \
    -XX:+OptimizeStringConcat -XX:+UseG1GC -XX:MinMetaspaceFreeRatio=5 -XX:MaxMetaspaceFreeRatio=10 \
    -XX:MinHeapFreeRatio=20 -XX:MaxHeapFreeRatio=40 -XX:+UseSerialGC \
    -Djava.util.concurrent.ForkJoinPool.common.parallelism=10 -XX:+ExitOnOutOfMemoryError \
    -XX:+UnlockDiagnosticVMOptions -XX:NativeMemoryTracking=summary -XX:CompressedClassSpaceSize=128M \
    -XX:ReservedCodeCacheSize=200M \
    -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap \
    -Djdk.tls.client.protocols=TLSv1.2 \
    -Djava.security.egd=file:/dev/./urandom ${JVM_OPTS} \
    -jar main.jar
